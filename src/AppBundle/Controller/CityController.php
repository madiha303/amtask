<?php
/**
 * Created by PhpStorm.
 * User: waqas716
 * Date: 08/02/2018
 * Time: 5:38 PM
 */

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Unirest\Request;
use Unirest\Request\Test\UnirestRequestTest;

class CityController extends Controller {
	/**
	 * @Route("/city/{name}")
	 */

	public function showAction( $name ) {



		//returning template: app/Resources/views/place/show.html.twig
//		return $this->render( 'place/show.html.twig', array(
//			'cityName' => $name,
//		) );

		// API call to http://api.openweathermap.org
		$headers = array('Accept' => 'application/json');
		$query = array('q' => $name, 'units'=>'metric', 'appid' => '8ca1bf554fe26dff41d635d4e2f866ed');
		$query2 = array('q' => 'cologne', 'units'=>'metric','appid' => '8ca1bf554fe26dff41d635d4e2f866ed');

		$response = Request::get('http://api.openweathermap.org/data/2.5/weather',$headers,$query);
		$response2 = Request::get('http://api.openweathermap.org/data/2.5/weather',$headers,$query2);


		//  check for naming condition
		$naming = (strlen($response->body->name)%2 ==0 ) ? true : false ;
		$currentTime=date('U');

		//  check for daytemp condition
		$dayStatus = (($currentTime > $response->body->sys->sunrise) && ($currentTime < $response->body->sys->sunset)) ? 'day' : 'night';
		$currentTemp= $response->body->main->temp;
		if($dayStatus=='day')
		{
			$daytemp =  (($currentTemp >17) && ($currentTemp < 25)) ? true : false;

		}elseif($dayStatus=='night'){

			$daytemp =  (($currentTemp >10) && ($currentTemp < 15)) ? true : false;

		}else{
			$daytemp = false;
		}

		//check if given city has temperature greater than cologne
		$rival =  ($response->body->main->temp > $response2->body->main->temp  ) ? true : false;


		// AND of all conditions
		$check = ($rival== true && $daytemp == true && $naming == true) ? true : false;


		$notes =['check' => $check, 'criteria' => ['naming' => $naming, 'daytemp' => $daytemp, 'rival' => $rival]];

		$data = [
			'result' => $notes
		];


//		return new JsonResponse($data);



		return $this->render('place/show.html.twig', array(
			'cityName' => $name,
            'jsonResponse' => json_encode( $data )
));
	}

	/**
	 * @Route("/city/{currentName}/notes", name="city_show_notes")
	 * @Method("GET")
	 */

	public function getCityAction($currentName){


// API call to http://api.openweathermap.org
$headers = array('Accept' => 'application/json');
$query = array('q' => $currentName, 'units'=>'metric', 'appid' => '8ca1bf554fe26dff41d635d4e2f866ed');
$query2 = array('q' => 'cologne', 'units'=>'metric','appid' => '8ca1bf554fe26dff41d635d4e2f866ed');

$response = Request::get('http://api.openweathermap.org/data/2.5/weather',$headers,$query);
$response2 = Request::get('http://api.openweathermap.org/data/2.5/weather',$headers,$query2);


		//  check for naming condition
		$naming = (strlen($response->body->name)%2 ==0 ) ? true : false ;
		$currentTime=date('U');

		//  check for daytemp condition
		$dayStatus = (($currentTime > $response->body->sys->sunrise) && ($currentTime < $response->body->sys->sunset)) ? 'day' : 'night';
		$currentTemp= $response->body->main->temp;
		if($dayStatus=='day')
		{
			$daytemp =  (($currentTemp >17) && ($currentTemp < 25)) ? true : false;

		}elseif($dayStatus=='night'){

			$daytemp =  (($currentTemp >10) && ($currentTemp < 15)) ? true : false;

		}else{
			$daytemp = false;
		}

		//check if given city has temperature greater than cologne
		$rival =  ($response->body->main->temp > $response2->body->main->temp  ) ? true : false;


		// AND of all conditions
		$check = ($rival== true && $daytemp == true && $naming == true) ? true : false;


	$notes =['check' => $check, 'criteria' => ['naming' => $naming, 'daytemp' => $daytemp, 'rival' => $rival]];

		$data = [
			'notes' => $notes
		];


		return new JsonResponse($data);

	}


}